package ru.acelost.masteringitemdecoration.decoration.dsl

import android.support.v7.widget.RecyclerView
import ru.acelost.masteringitemdecoration.decoration.Decoration
import ru.acelost.masteringitemdecoration.decoration.drawer.SolidDecorationDrawer
import ru.acelost.masteringitemdecoration.decoration.offset.BaseOffsetProvider
import ru.acelost.masteringitemdecoration.decoration.predicate.viewtype.IncludeViewTypePredicate

// region Recycler View extensions

/**
 * Добавить декорацию к RecyclerView.
 * @param block - блок, конфигурирующий декорацию
 */
inline infix fun RecyclerView.decorate(block: Decoration.() -> Unit): RecyclerView {
    // Создаем и конфигурируем декорацию
    val decoration = Decoration().apply(block)
    // Добавляем декорацию
    addItemDecoration(decoration)
    return this
}

// endregion

// region Decoration property extensions

/**
 * Задать объект, реализующий один или несколько аспектов декорирования.
 */
inline infix fun Decoration.drawer(create: () -> Any): Decoration {
    setDrawer(create())
    return this
}

/**
 * Задать отрисовщик декорации до отрисовки элемента списка.
 */
inline infix fun Decoration.beforeDrawer(create: () -> Decoration.BeforeDrawer): Decoration {
    setBeforeDrawer(create())
    return this
}

/**
 * Задать отрисовщик декорации после отрисовки элемента списка.
 */
inline infix fun Decoration.afterDrawer(create: () -> Decoration.AfterDrawer): Decoration {
    setAfterDrawer(create())
    return this
}

/**
 * Задать делегат условия выбора элементов для декорирования.
 */
inline fun Decoration.predicate(strategy: Decoration.Predicate.Strategy = Decoration.Predicate.Strategy.AND,
                                      create: () -> Decoration.Predicate): Decoration {
    setPredicate(create(), strategy)
    return this
}

// endregion

// region Utility decoration extensions

/**
 * Задать отступы для элемента.
 */
inline infix fun Decoration.offsets(block: BaseOffsetProvider.() -> Unit): Decoration {
    setOffsets(BaseOffsetProvider().apply(block))
    return this
}

/**
 * Задать заливку для декорации.
 */
inline infix fun Decoration.solid(block: SolidDecorationDrawer.() -> Unit): Decoration {
    setAfterDrawer(SolidDecorationDrawer.After().apply(block))
    return this
}

/**
 * Задать декорируемый тип.
 */
fun Decoration.viewType(viewType: Int): Decoration {
    return predicate(Decoration.Predicate.Strategy.AND) { IncludeViewTypePredicate(viewType) }
}

/**
 * Задать декорируемый тип.
 */
inline fun Decoration.viewType(viewType: () -> Int): Decoration {
    return viewType(viewType())
}

/**
 * Задать перечень декорироруемых типов.
 */
fun Decoration.viewTypes(vararg types: Int): Decoration {
    return predicate(Decoration.Predicate.Strategy.AND) { IncludeViewTypePredicate(*types) }
}

/**
 * Задать перечень декорироруемых типов.
 */
inline fun Decoration.viewTypes(types: () -> IntArray): Decoration {
    return viewTypes(*types())
}

// endregion