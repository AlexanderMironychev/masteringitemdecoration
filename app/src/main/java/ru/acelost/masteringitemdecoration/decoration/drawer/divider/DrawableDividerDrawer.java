package ru.acelost.masteringitemdecoration.decoration.drawer.divider;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;
import ru.acelost.masteringitemdecoration.decoration.drawer.AlphaDecorationDrawer;

/**
 * Отрисовщик разделителя элемента списка. В качестве разделителя используется указанный drawable.
 */
public class DrawableDividerDrawer extends AlphaDecorationDrawer
        implements Decoration.AfterDrawer, Decoration.ItemOffsetProvider {

    /**
     * Drawable для разделителя.
     */
    @NonNull
    private final Drawable mDrawable;

    public DrawableDividerDrawer(@NonNull Context context, int drawableRes) {
        this(context, drawableRes, DEFAULT_ADJUST_ALPHA);
    }

    public DrawableDividerDrawer(@NonNull Context context, int drawableRes, boolean adjustAlpha) {
        this(compatDrawable(context, drawableRes), adjustAlpha);
    }

    public DrawableDividerDrawer(@NonNull Drawable drawable, boolean adjustAlpha) {
        super(adjustAlpha);
        mDrawable = drawable;
    }

    @NonNull
    private static Drawable compatDrawable(@NonNull Context context, int drawableRes) {
        return require(ContextCompat.getDrawable(context, drawableRes));
    }

    @NonNull
    private static Drawable require(Drawable drawable) {
        if (drawable == null) {
            throw new IllegalArgumentException("Drawable is null!");
        }
        return drawable;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        // Получаем отступы для декорации на основе размера drawable
        outRect.set(0, 0, 0, mDrawable.getIntrinsicHeight());
    }

    @Override
    public void drawAfter(@NonNull Canvas canvas, int childIndex, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state, int left, int top, int right, int bottom, @NonNull Rect offsets) {
        mDrawable.setBounds(left, bottom - offsets.bottom, right, bottom);
        mDrawable.setAlpha(getAdjustAlpha() ? intAlpha(child) : ALPHA_OPAQUE);
        mDrawable.draw(canvas);
    }

}
