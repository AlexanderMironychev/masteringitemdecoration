package ru.acelost.masteringitemdecoration.decoration.predicate;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;

/**
 * Реализация выбора элементов для декорирования на основе view type.
 */
public abstract class ViewTypePredicate implements Decoration.Predicate {

    /**
     * Нужно ли декорировать view указанного типа.
     *
     * @param viewType  - тип view
     * @param child     - view для декорации
     * @param parent    - родительский recycler view
     * @param state     - состояние recycler view
     * @return true - если элемент нужно декорировать, false - иначе
     */
    protected abstract boolean needToDecorateViewHolder(int viewType, @NonNull View child,
                                                        @NonNull RecyclerView parent,
                                                        @NonNull RecyclerView.State state);

    @Override
    public boolean needToDecorate(@NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        final RecyclerView.ViewHolder holder = parent.getChildViewHolder(child);
        if (holder != null) {
            return needToDecorateViewHolder(holder.getItemViewType(), child, parent, state);
        }
        return false;
    }

}
