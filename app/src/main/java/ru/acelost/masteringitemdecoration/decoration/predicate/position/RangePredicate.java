package ru.acelost.masteringitemdecoration.decoration.predicate.position;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.PositionPredicate;

/**
 * Реализация выбора элементов декорирования на основе диапазона позиций в списке.
 */
public class RangePredicate extends PositionPredicate {

    /**
     * Начиная с какой позиции необходимо декорировать.
     */
    private final int mStart;

    /**
     * До какой позиции необходимо декорировать.
     */
    private final int mEnd;

    public RangePredicate(int start, int end) {
        mStart = start;
        mEnd = end;
    }

    @Override
    protected boolean needToDecorateItem(int adapterPosition, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        return mStart <= adapterPosition && adapterPosition < mEnd;
    }

}
