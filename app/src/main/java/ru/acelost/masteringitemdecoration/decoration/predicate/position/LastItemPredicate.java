package ru.acelost.masteringitemdecoration.decoration.predicate.position;

/**
 * Реализация выбора последнего элемента для декорирования.
 */
public class LastItemPredicate extends TailPredicate {

    public LastItemPredicate() {
        super(1);
    }

}
