package ru.acelost.masteringitemdecoration

import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import ru.acelost.masteringitemdecoration.decoration.drawer.divider.SolidDividerDrawer
import ru.acelost.masteringitemdecoration.decoration.dsl.decorate
import ru.acelost.masteringitemdecoration.decoration.dsl.drawer
import ru.acelost.masteringitemdecoration.decoration.dsl.predicate
import ru.acelost.masteringitemdecoration.decoration.predicate.position.LastItemPredicate

class SampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)
        val list = findViewById<RecyclerView>(R.id.list_view)
        list.layoutManager = LinearLayoutManager(this)
        list.adapter = SampleAdapter(15)

        list.decorate {
            drawer { SolidDividerDrawer(Color.RED, false, 50) }
            predicate { LastItemPredicate() }
        }
    }

}
