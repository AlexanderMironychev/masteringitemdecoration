package ru.acelost.masteringitemdecoration.decoration.predicate.position;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.PositionPredicate;

/**
 * Реализация выбора указанного количества элементов с конца списка для декорирования.
 */
public class TailPredicate extends PositionPredicate {

    /**
     * Сколько элементов с конца декорировать.
     */
    private final int mTail;

    public TailPredicate(int tail) {
        mTail = tail;
    }

    @Override
    protected boolean needToDecorateItem(int adapterPosition, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        return adapterPosition >= state.getItemCount() - mTail;
    }

}
