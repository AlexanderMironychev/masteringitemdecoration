package ru.acelost.masteringitemdecoration.decoration.drawer;

import android.support.annotation.Nullable;
import android.view.View;

/**
 * Базовая реализация отрисовщика декорации с изменением прозрачности.
 */
public abstract class AlphaDecorationDrawer {

    /**
     * Нужно ли применять прозрачность по-умолчанию.
     */
    protected static final boolean DEFAULT_ADJUST_ALPHA = true;

    /**
     * Значение для абсолютной непрозрачности.
     */
    protected static final int ALPHA_OPAQUE = 255;

    /**
     * Нужно ли применять прозрачность.
     */
    private boolean mAdjustAlpha;

    public AlphaDecorationDrawer() {
        this(DEFAULT_ADJUST_ALPHA);
    }

    public AlphaDecorationDrawer(boolean adjustAlpha) {
        mAdjustAlpha = adjustAlpha;
    }

    public boolean getAdjustAlpha() {
        return mAdjustAlpha;
    }

    public void setAdjustAlpha(boolean adjustAlpha) {
        mAdjustAlpha = adjustAlpha;
    }

    /**
     * Целочисленное значение для прозрачности на основе прозрачности view.
     *
     * @param view - view, на основе которого получаем целочисленную прозрачность
     * @return целочисленное [0-255] значение прозрачности
     */
    protected static int intAlpha(@Nullable View view) {
        if (view == null) {
            return 0;
        }
        return (int) (view.getAlpha() * ALPHA_OPAQUE);
    }

}
