package ru.acelost.masteringitemdecoration.decoration.predicate.composition;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;

/**
 * Вспомогательная реализация выбора элементов для декорирования,
 * применяющая логическую связку OR для указанных {@link Decoration.Predicate}.
 */
public class OrPredicate implements Decoration.Predicate {

    public Decoration.Predicate[] mPredicates;

    public OrPredicate(Decoration.Predicate... predicates) {
        mPredicates = predicates;
    }

    @Override
    public boolean needToDecorate(@NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        for (Decoration.Predicate predicate : mPredicates) {
            if (predicate.needToDecorate(child, parent, state)) {
                return true;
            }
        }
        return false;
    }

}