package ru.acelost.masteringitemdecoration.decoration;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.composition.AndPredicate;
import ru.acelost.masteringitemdecoration.decoration.predicate.composition.OrPredicate;

/**
 * Базовый класс для декорации элементов {@link RecyclerView},
 * использующий композицию вместо наследования для реализации конкретной декорации.
 * Разбивает декорирование элементов списка на 4 аспекта,
 * реализуемые при помощи 4 вспомогательных интерфейсов:
 * - Отступы элемента {@link ItemOffsetProvider};
 * - Отрисовка декорации ДО отрисовки элемента списка {@link BeforeDrawer};
 * - Отрисовка декорации ПОСЛЕ отрисовки элемента списка {@link AfterDrawer};
 * - Выбор элементов, которые нужно декорировать {@link Predicate}.
 *
 * Данный подход позволяет переиспользовать реализации отдельных аспектов, а так же гибко
 * реализовывать отдельные аспекты с возможностью наследования - внутри аспекта.
 * Для каждого аспекта есть отдельный setter, поддерживающий BuilderStyle. (прим. {@link #setPredicate(Predicate)}).
 * Так же есть setter для объекта, реализующего сразу несколько аспектов - {@link #setDrawer(Object)}.
 */
public class Decoration extends RecyclerView.ItemDecoration {

    /**
     * Флаг, декларирующий этап отрисовки декорации до отрисовки элемента списка.
     */
    public static final int DRAW_BEFORE = -1;

    /**
     * Флаг, декларирующий этап отрисовки декорации после отрисовки элемента списка.
     */
    public static final int DRAW_AFTER = 1;

    /**
     * Аннотация, ограничивающая возможные значения для этапа отрисовки.
     */
    @IntDef({DRAW_BEFORE, DRAW_AFTER})
    public @interface DrawStage { }

    // region Decoration configuration

    /**
     * Объект, отвечающий за размер отступов у элемента списка.
     */
    @Nullable
    private ItemOffsetProvider mOffsetProvider;

    /**
     * Объект, выполняющий отрисовку декорации до отрисовки элемента списка.
     */
    @Nullable
    private BeforeDrawer mBeforeDrawer;

    /**
     * Объект, выполняющий отрисовку декорации после отрисовки элемента списка.
     */
    @Nullable
    private AfterDrawer mAfterDrawer;

    /**
     * Объект, отвечающий за выбор элементов, которые необходимо декорировать.
     */
    @Nullable
    private Predicate mPredicate;

    // endregion

    // region Cache fields

    private final Rect mItemBounds = new Rect();

    private final Rect mItemOffsets = new Rect();

    // endregion

    // region Setters

    /**
     * Получить объект, отвечающий за размер отступов у элементов списка.
     */
    @Nullable
    public ItemOffsetProvider getOffsets() {
        return mOffsetProvider;
    }

    /**
     * Задать объект, отвечающий за размер отступов у элементов списка.
     */
    public Decoration setOffsets(@Nullable ItemOffsetProvider provider) {
        mOffsetProvider = provider;
        return this;
    }

    /**
     * Получить отрисовщик декорации до отрисовки элемента списка.
     */
    @Nullable
    public BeforeDrawer getBeforeDrawer() {
        return mBeforeDrawer;
    }

    /**
     * Задать отрисовщик декорации до отрисовки элемента списка.
     */
    public Decoration setBeforeDrawer(@Nullable BeforeDrawer drawer) {
        mBeforeDrawer = drawer;
        return this;
    }

    /**
     * Получить отрисовщик декорации после отрисовки элемента списка.
     */
    @Nullable
    public AfterDrawer getAfterDrawer() {
        return mAfterDrawer;
    }

    /**
     * Задать отрисовщик декорации после отрисовки элемента списка.
     */
    public Decoration setAfterDrawer(@Nullable AfterDrawer drawer) {
        mAfterDrawer = drawer;
        return this;
    }

    /**
     * Получить объект, отвечающий за выбор элементов, которые необходимо декорировать.
     */
    @Nullable
    public Predicate getPredicate() {
        return mPredicate;
    }

    /**
     * Задать объект, отвечающий за выбор элементов, которые необходимо декорировать.
     */
    public Decoration setPredicate(@Nullable Predicate predicate) {
        return setPredicate(predicate, Predicate.Strategy.REPLACE);
    }

    /**
     * Применить объект, отвечающий за выбор элементов, которые необходимо декорировать.
     *
     * @param predicate - предикат, который необходимо применить
     * @param strategy  - стратегия применения предиката
     */
    public Decoration setPredicate(@Nullable Predicate predicate, @NonNull Predicate.Strategy strategy) {
        mPredicate = strategy.apply(mPredicate, predicate);
        return this;
    }

    /**
     * Задать объект, реализующий один или несколько аспектов декорирования.
     * Например, если объект реализует отрисовку до и после элемента, - данный метод
     * воспримет drawer и как {@link BeforeDrawer}, и как {@link AfterDrawer} одновременно.
     *
     * @param drawer - объект, реализующий несколько аспектов декорирования.
     */
    public Decoration setDrawer(@NonNull Object drawer) {
        boolean used = false;
        if (drawer instanceof BeforeDrawer) {
            setBeforeDrawer((BeforeDrawer) drawer);
            used = true;
        }
        if (drawer instanceof AfterDrawer) {
            setAfterDrawer((AfterDrawer) drawer);
            used = true;
        }
        if (drawer instanceof ItemOffsetProvider) {
            setOffsets((ItemOffsetProvider) drawer);
            used = true;
        }
        if (drawer instanceof Predicate) {
            setPredicate((Predicate) drawer);
            used = true;
        }
        if (!used) {
            // Делегат должен реализовывать хотя бы один аспект декорирования
            throw new IllegalArgumentException("Delegate " + drawer + " should implement any companion interfaces.");
        }
        return this;
    }

    // endregion

    // region RecyclerView.ItemDecoration impl

    /**
     * Нужно ли декорировать элемент списка.
     *
     * @param child     - view элемента
     * @param parent    - родительский recycler view
     * @param state     - состояние recycler view
     * @return true - если необходимо декорировать элемент, false - иначе
     */
    protected boolean needToDecorate(@NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        return mPredicate == null || mPredicate.needToDecorate(child, parent, state);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mOffsetProvider != null && needToDecorate(view, parent, state)) {
            // Если указан делегат для отступов и элемент необходимо декорировать
            getItemOffsetsInternal(outRect, view, parent, state);
        } else {
            outRect.setEmpty();
        }
    }

    protected void getItemOffsetsInternal(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if (mOffsetProvider != null) {
            mOffsetProvider.getItemOffsets(outRect, view, parent, state);
        } else {
            outRect.setEmpty();
        }
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mBeforeDrawer != null) {
            // Выполняем отрисовку декорации до отрисовки элемента списка
            drawForEach(DRAW_BEFORE, c, parent, state);
        }
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mAfterDrawer != null) {
            // Выполняем отрисовку декорации после отрисовки элемента списка
            drawForEach(DRAW_AFTER, c, parent, state);
        }
    }

    /**
     * Выполнить отрисовку декорации для всех элементов, которые этого требуют.
     *
     * @param stage     - этап отрисовки (до или после отрисовки элемента)
     * @param canvas    - canvas для отрисовки
     * @param parent    - родительский recycler view
     * @param state     - состояние recycler view
     */
    private void drawForEach(@DrawStage int stage,
                             @NonNull Canvas canvas,
                             @NonNull RecyclerView parent,
                             @NonNull RecyclerView.State state) {
        canvas.save();
        final int left;
        final int right;
        if (parent.getClipToPadding()) {
            // Ограничиваем канвас для того, чтобы предотвратить
            // отрисовку декорации за границами recycler view
            left = parent.getPaddingLeft();
            right = parent.getWidth() - parent.getPaddingRight();
            canvas.clipRect(left, parent.getPaddingTop(), right,
                    parent.getHeight() - parent.getPaddingBottom());
        } else {
            left = 0;
            right = parent.getWidth();
        }

        final int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = parent.getChildAt(i);
            if (!needToDecorate(child, parent, state)) {
                // Элемент не нужно декорировать
                continue;
            }
            // Получаем значение отступов для элемента списка
            getItemOffsetsInternal(mItemOffsets, child, parent, state);
            // Получаем границы элемента списка
            parent.getDecoratedBoundsWithMargins(child, mItemBounds);
            final int translationY = Math.round(child.getTranslationY());
            // Применяем смещение по вертикали
            final int top = mItemBounds.top + translationY;
            final int bottom = mItemBounds.bottom + translationY;
            // Отрисовываем декорацию для элемента списка
            drawForItem(stage, canvas, i, child, parent, state, left, top, right, bottom, mItemOffsets);
        }
        canvas.restore();
    }

    /**
     * Выполнить отрисовку декорации для элемента списка.
     *
     * @param stage         - этап отрисовки (до или после отрисовки элемента)
     * @param canvas        - canvas для отрисовки декорации
     * @param childIndex    - индекс view элемента внутри recycler view
     * @param child         - view элемента внутри recycler view
     * @param parent        - родительский recycler view
     * @param state         - состояние recycler view
     * @param left          - левая граница для декорации
     * @param top           - верхняя граница для декорации
     * @param right         - правая граница для декорации
     * @param bottom        - нижняя граница для декорации
     * @param offsets       - отступы для декорирования
     */
    private void drawForItem(@DrawStage int stage,
                             @NonNull Canvas canvas,
                             int childIndex,
                             @NonNull View child,
                             @NonNull RecyclerView parent,
                             @NonNull RecyclerView.State state,
                             int left, int top, int right, int bottom,
                             @NonNull Rect offsets) {
        if (stage == DRAW_BEFORE) {
            if (mBeforeDrawer != null) {
                mBeforeDrawer.drawBefore(canvas, childIndex, child, parent, state, left, top, right, bottom, offsets);
            }
        } else if (stage == DRAW_AFTER) {
            if (mAfterDrawer != null) {
                mAfterDrawer.drawAfter(canvas, childIndex, child, parent, state, left, top, right, bottom, offsets);
            }
        }
    }

    // endregion

    // region Companion Interfaces

    /**
     * Интерфейс объекта, отвечающего за размер отступов у элемента списка.
     */
    public interface ItemOffsetProvider {

        /**
         * Получить отступы для элемента списка.
         *
         * @param outRect   - объект, в который нужно передать отступы
         * @param view      - view для которого вычисляем отступы
         * @param parent    - родительский recycler view
         * @param state     - состояние recycler view
         */
        void getItemOffsets(@NonNull Rect outRect, @NonNull View view,
                            @NonNull RecyclerView parent, @NonNull RecyclerView.State state);

    }

    /**
     * Интерфейс отрисовщика декорации до отрисовки элемента списка.
     */
    public interface BeforeDrawer {

        /**
         * Отрисовать декорацию до отрисовки элемента списка.
         *
         * @param canvas        - canvas для отрисовки декорации
         * @param childIndex    - индекс view элемента списка внутри recycler view
         * @param child         - view элемента списка внутри recycler view
         * @param parent        - родительский recycler view
         * @param state         - состояние recycler view
         * @param left          - левая граница для декорации
         * @param top           - верхняя граница для декорации
         * @param right         - правая граница для декорации
         * @param bottom        - нижняя граница для декорации
         * @param offsets       - отступы для декорирования
         */
        void drawBefore(@NonNull Canvas canvas, int childIndex, @NonNull View child,
                        @NonNull RecyclerView parent, @NonNull RecyclerView.State state,
                        int left, int top, int right, int bottom, @NonNull Rect offsets);

    }

    /**
     * Интерфейс отрисовщика декорации после отрисовки элемента списка.
     */
    public interface AfterDrawer {

        /**
         * Отрисовать декорацию после отрисовки элемента списка.
         *
         * @param canvas        - canvas для отрисовки декорации
         * @param childIndex    - индекс view элемента списка внутри recycler view
         * @param child         - view элемента списка внутри recycler view
         * @param parent        - родительский recycler view
         * @param state         - состояние recycler view
         * @param left          - левая граница для декорации
         * @param top           - верхняя граница для декорации
         * @param right         - правая граница для декорации
         * @param bottom        - нижняя граница для декорации
         * @param offsets       - отступы для декорирования
         */
        void drawAfter(@NonNull Canvas canvas, int childIndex, @NonNull View child,
                       @NonNull RecyclerView parent, @NonNull RecyclerView.State state,
                       int left, int top, int right, int bottom, @NonNull Rect offsets);

    }

    /**
     * Интерфейс объекта, отвечающего за выбор элементов списка, которые нужно декорировать.
     */
    public interface Predicate {

        /**
         * Нужно ли декорировать элемент списка.
         *
         * @param child     - view элемента
         * @param parent    - родительский recycler view
         * @param state     - состояние recycler view
         * @return true - если элемент нужно декорировать, false - иначе
         */
        boolean needToDecorate(@NonNull View child, @NonNull RecyclerView parent,
                               @NonNull RecyclerView.State state);

        /**
         * Стратегия применения предиката.
         */
        enum Strategy {
            /**
             * Заменяем предикат.
             */
            REPLACE {
                @Nullable
                @Override
                public Predicate apply(@Nullable Predicate current, @Nullable Predicate recent) {
                    return recent;
                }
            },
            /**
             * Связываем предикаты логическим AND.
             */
            AND {
                @Nullable
                @Override
                public Predicate apply(@Nullable Predicate current, @Nullable Predicate recent) {
                    if (current == null) {
                        return recent;
                    }
                    if (recent == null) {
                        return current;
                    }
                    return new AndPredicate(current, recent);
                }
            },
            /**
             * Связываем предикаты логическим OR.
             */
            OR {
                @Nullable
                @Override
                public Predicate apply(@Nullable Predicate current, @Nullable Predicate recent) {
                    if (current == null) {
                        return recent;
                    }
                    if (recent == null) {
                        return current;
                    }
                    return new OrPredicate(current, recent);
                }
            };

            /**
             * Применить новый предикат к текущему.
             *
             * @param current   - текущий предикат
             * @param recent    - новый предикат
             * @return результат применения нового предиката
             */
            @Nullable
            public abstract Predicate apply(@Nullable Predicate current, @Nullable Predicate recent);
        }

    }

    // endregion

}
