package ru.acelost.masteringitemdecoration.decoration.predicate.position;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.PositionPredicate;

/**
 * Реализация выбора элементов декорирования на основе фиксированной позиций в списке.
 */
public class SinglePositionPredicate extends PositionPredicate {

    /**
     * Позиция, которую необходимо декорировать.
     */
    private final int mPosition;

    public SinglePositionPredicate(int position) {
        mPosition = position;
    }

    @Override
    protected boolean needToDecorateItem(int adapterPosition, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        return adapterPosition == mPosition;
    }

}
