package ru.acelost.masteringitemdecoration.decoration.predicate.position;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.PositionPredicate;

/**
 * Реализация выбора элементов декорирования с применением отступов от начала и от конца списка.
 */
public class OffsetPredicate extends PositionPredicate {

    /**
     * Начиная с какого элемента необходимо декорировать.
     */
    private final int mStartOffset;

    /**
     * Сколько элементов с конца не нужно декорировать
     */
    private final int mEndOffset;

    public OffsetPredicate(int startOffset, int endOffset) {
        mStartOffset = startOffset;
        mEndOffset = endOffset;
    }

    @Override
    protected boolean needToDecorateItem(int adapterPosition, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        return mStartOffset <= adapterPosition && adapterPosition < state.getItemCount() - mEndOffset;
    }

}
