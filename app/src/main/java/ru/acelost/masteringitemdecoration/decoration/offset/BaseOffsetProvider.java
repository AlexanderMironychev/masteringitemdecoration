package ru.acelost.masteringitemdecoration.decoration.offset;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;

/**
 * Базовая реализация отступов для декорации.
 */
public class BaseOffsetProvider implements Decoration.ItemOffsetProvider {

    private int mLeft, mTop, mRight, mBottom;

    public BaseOffsetProvider() { }

    public BaseOffsetProvider(int left, int top, int right, int bottom) {
        mLeft = left;
        mTop = top;
        mRight = right;
        mBottom = bottom;
    }

    public int getLeft() {
        return mLeft;
    }

    public void setLeft(int left) {
        mLeft = left;
    }

    public int getTop() {
        return mTop;
    }

    public void setTop(int top) {
        mTop = top;
    }

    public int getRight() {
        return mRight;
    }

    public void setRight(int right) {
        mRight = right;
    }

    public int getBottom() {
        return mBottom;
    }

    public void setBottom(int bottom) {
        mBottom = bottom;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        outRect.set(mLeft, mTop, mRight, mBottom);
    }

}
