package ru.acelost.masteringitemdecoration.decoration.offset;

/**
 * Реализация отступа для декорации только снизу. Применима,
 * например, для разделителей в списке.
 */
public class BottomOffsetProvider extends BaseOffsetProvider {

    public BottomOffsetProvider(int offset) {
        super(0, 0, 0, offset);
    }

}
