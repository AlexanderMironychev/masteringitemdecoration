package ru.acelost.masteringitemdecoration.decoration.predicate.viewtype;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.ViewTypePredicate;

/**
 * Реализация выбора элементов для декорирования на основе недопустимых view type.
 */
public class ExcludeViewTypePredicate extends ViewTypePredicate {

    /**
     * Недопустимые типы view.
     */
    private int[] mViewTypes;

    public ExcludeViewTypePredicate(int... viewTypes) {
        mViewTypes = viewTypes;
    }

    @Override
    protected boolean needToDecorateViewHolder(int viewType, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        for (int includingType : mViewTypes) {
            if (viewType == includingType) {
                return false;
            }
        }
        return true;
    }

}