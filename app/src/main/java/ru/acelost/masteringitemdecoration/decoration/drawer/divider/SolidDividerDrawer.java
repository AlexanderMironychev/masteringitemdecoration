package ru.acelost.masteringitemdecoration.decoration.drawer.divider;

import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;
import ru.acelost.masteringitemdecoration.decoration.drawer.SolidDecorationDrawer;

/**
 * Отрисовщик разделителя элемента списка. В качестве разделителя используется заливка указанным цветом.
 */
public class SolidDividerDrawer extends SolidDecorationDrawer.After implements Decoration.ItemOffsetProvider {

    /**
     * Размер разделителя.
     */
    private final int mWidth;

    public SolidDividerDrawer(int color, int width) {
        this(color, DEFAULT_ADJUST_ALPHA, width);
    }

    public SolidDividerDrawer(int color, boolean adjustAlpha, int width) {
        setColor(color);
        setAdjustAlpha(adjustAlpha);
        mWidth = width;
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        // Задаем отступ снизу для отрисовки разделителя
        //noinspection SuspiciousNameCombination
        outRect.set(0, 0, 0, mWidth);
    }

}
