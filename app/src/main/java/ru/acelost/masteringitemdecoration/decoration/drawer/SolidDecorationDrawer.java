package ru.acelost.masteringitemdecoration.decoration.drawer;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;

/**
 * Реализация отрисовщика декорации, заливающего отступы указанным цветом.
 */
public class SolidDecorationDrawer extends AlphaDecorationDrawer {

    /**
     * Цвет заливки по-умолчанию.
     */
    protected static final int DEFAULT_SOLID_COLOR = Color.TRANSPARENT;

    /**
     * Кисть для выполнения заливки.
     */
    @NonNull
    private final Paint mPaint = new Paint();

    /**
     * Цвет заливки.
     */
    private int mColor;

    public SolidDecorationDrawer() {
        this(DEFAULT_SOLID_COLOR, DEFAULT_ADJUST_ALPHA);
    }

    public SolidDecorationDrawer(int color) {
        this(color, DEFAULT_ADJUST_ALPHA);
    }

    public SolidDecorationDrawer(int color, boolean adjustAlpha) {
        super(adjustAlpha);
        setColor(color);
    }

    /**
     * Получить цвет заливки.
     * @return цвет заливки
     */
    public int getColor() {
        return mColor;
    }

    /**
     * Задать цвет заливки.
     * @param color - цвет заливки
     */
    public void setColor(int color) {
        mColor = color;
    }

    /**
     * Залить отступы view.
     *
     * @param canvas    - canvas для выполнения заливки
     * @param child     - view для которого выполняется заливка
     * @param left      - левая граница для декорации
     * @param top       - верхняя граница для декорации
     * @param right     - правая граница для декорации
     * @param bottom    - нижняя граница для декорации
     * @param offsets   - отступы для декорирования
     */
    protected void drawSolid(@NonNull Canvas canvas, @NonNull View child,
                             int left, int top, int right, int bottom, @NonNull Rect offsets) {
        preparePaint(mPaint, child);
        if (offsets.top > 0) {
            canvas.drawRect(left, top, right, top + offsets.top, mPaint);
        }
        if (offsets.bottom > 0) {
            canvas.drawRect(left, bottom - offsets.bottom, right, bottom, mPaint);
        }
        if (offsets.left > 0) {
            canvas.drawRect(left, top + offsets.top, left + offsets.left, bottom - offsets.bottom, mPaint);
        }
        if (offsets.right > 0) {
            canvas.drawRect(right - offsets.right, top + offsets.top, right, bottom - offsets.bottom, mPaint);
        }
    }

    /**
     * Подготавливаем кисть для заливки.
     *
     * @param paint - кисть, которую необходимо подготовить
     * @param view  - view для которого будет выполняться заливка
     */
    private void preparePaint(@NonNull Paint paint, @NonNull View view) {
        paint.setColor(mColor);
        if (getAdjustAlpha()) {
            paint.setAlpha(intAlpha(view));
        } else {
            paint.setAlpha(ALPHA_OPAQUE);
        }
    }

    /**
     * Реализация {@link SolidDecorationDrawer} для выполнения заливки до отрисовки элемента списка.
     */
    public static class Before extends SolidDecorationDrawer implements Decoration.BeforeDrawer {

        @Override
        public void drawBefore(@NonNull Canvas canvas, int childIndex, @NonNull View child,
                               @NonNull RecyclerView parent, @NonNull RecyclerView.State state,
                               int left, int top, int right, int bottom, @NonNull Rect offsets) {
            drawSolid(canvas, child, left, top, right, bottom, offsets);
        }

    }

    /**
     * Реализация {@link SolidDecorationDrawer} для выполнения заливки после отрисовки элемента списка.
     */
    public static class After extends SolidDecorationDrawer implements Decoration.AfterDrawer {

        @Override
        public void drawAfter(@NonNull Canvas canvas, int childIndex, @NonNull View child,
                              @NonNull RecyclerView parent, @NonNull RecyclerView.State state,
                              int left, int top, int right, int bottom, @NonNull Rect offsets) {
            drawSolid(canvas, child, left, top, right, bottom, offsets);
        }

    }

}
