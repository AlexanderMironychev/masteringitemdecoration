package ru.acelost.masteringitemdecoration.decoration.predicate;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.Decoration;

/**
 * Реализация выбора элементов для декорирования на основе их позиции в списке.
 */
public abstract class PositionPredicate implements Decoration.Predicate {

    /**
     * Нужно ли декорировать view на указанной позиции.
     *
     * @param adapterPosition   - позиция элемента в списке
     * @param child             - view элемента
     * @param parent            - родительский recycler view
     * @param state             - состояние recycler view
     * @return true - если элемент нужно декорировать, false - иначе
     */
    protected abstract boolean needToDecorateItem(int adapterPosition, @NonNull View child, @NonNull RecyclerView parent,
                                                  @NonNull RecyclerView.State state);

    @Override
    public final boolean needToDecorate(@NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        // Получаем позицию элемента в списке
        final int adapterPosition = parent.getChildAdapterPosition(child);
        return needToDecorateItem(adapterPosition, child, parent, state);
    }

}
