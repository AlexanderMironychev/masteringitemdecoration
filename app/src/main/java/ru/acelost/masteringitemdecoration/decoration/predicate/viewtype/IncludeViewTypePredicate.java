package ru.acelost.masteringitemdecoration.decoration.predicate.viewtype;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import ru.acelost.masteringitemdecoration.decoration.predicate.ViewTypePredicate;

/**
 * Реализация выбора элементов для декорирования на основе допустимых view type.
 */
public class IncludeViewTypePredicate extends ViewTypePredicate {

    /**
     * Допустимые типы view.
     */
    private int[] mViewTypes;

    public IncludeViewTypePredicate(int... viewTypes) {
        mViewTypes = viewTypes;
    }

    @Override
    protected boolean needToDecorateViewHolder(int viewType, @NonNull View child, @NonNull RecyclerView parent, @NonNull RecyclerView.State state) {
        for (int includingType : mViewTypes) {
            if (viewType == includingType) {
                return true;
            }
        }
        return false;
    }

}
